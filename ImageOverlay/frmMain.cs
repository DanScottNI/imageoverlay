﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace ImageOverlay
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            this.InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (ofdOpenFiles.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(ofdOpenFiles.FileName);
            }
        }

        private void scrOpacity_Scroll(object sender, ScrollEventArgs e)
        {
            this.RefreshOpacity();
        }

        private void RefreshOpacity()
        {
            this.Opacity = Convert.ToDouble(scrOpacity.Value) / 100;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            scrOpacity.Value = Convert.ToInt32(this.Opacity * 100);
            this.RefreshOpacity();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            scrOpacity.Value = scrOpacity.Maximum;
            this.RefreshOpacity();
        }

        private void chkStayOnTop_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = chkStayOnTop.Checked;
        }
    }
}
